const {src, dest, parallel, series, watch} = require('gulp');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');

function browsersync() {
    browserSync.init({
        server: {baseDir: 'app/'},
    })
}

function styles() {
    return src('app/sass/main.sass')
        .pipe(sass())
        .pipe(concat('app.min.css'))
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 10 versions']
        }))
        .pipe(dest('app/css/'))
        .pipe(browserSync.stream())
}

function startWatch() {
    watch('app/**/' + 'sass' + '/**/*', styles);
    watch('app/**/*.html').on('change', browserSync.reload)
}

function build() {
    return src([
        'app/css/**/*.min.css',
        'app/**/*.html'
    ])
        .pipe(dest('dist'));
}

exports.browsersync = browsersync;
exports.styles = styles;
exports.build = series(styles, build)

exports.default = parallel(styles, browsersync, startWatch)